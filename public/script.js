
function getPrices() {
    return {
      prodTypes: [90, 0, 0],
      prodOptions: {
        option1: 87,
        option2: 86,
        option3: 92,
      },
      prodProperties: {
        prop1: 100,
        prop2: 80,
      }
    }
  }

function updatePrice() {
  let s = document.getElementsByName("prodType");
  let amountg = document.getElementById("amountg").value;
  let select = s[0];
  let price = 0;
  let prices = getPrices();
  let priceIndex = parseInt(select.value) - 1;
  if (priceIndex >= 0) {
    price = prices.prodTypes[priceIndex];
  }

  let radioDiv = document.getElementById("radios");
  radioDiv.style.display = (select.value == "2" ? "block" : "none");
  let radios = document.getElementsByName("prodOptions");
  radios.forEach(function (radio) {
    if (radio.checked) {
      let optionPrice = prices.prodOptions[radio.value];
      if (optionPrice !== undefined && select.value == "2") {
        price += optionPrice;
      }
    }
  });


  let checkDiv = document.getElementById("checkboxes");
  checkDiv.style.display = (select.value == "3" ? "block" : "none");
  let checkboxes = document.querySelectorAll("#checkboxes input");
  checkboxes.forEach(function (checkbox) {
    if (checkbox.checked) {
      let propPrice = prices.prodProperties[checkbox.name];
      if (propPrice !== undefined && select.value == "3") {
        price += propPrice;
      }
    }
  });


  let prodPrice = document.getElementById("prodPrice");
  prodPrice.innerHTML = price * amountg + " p";
}

window.addEventListener("DOMContentLoaded", function (event) {
  let radioDiv = document.getElementById("radios");
  radioDiv.style.display = "none";
  let number = document.getElementById("amountg");
  number.addEventListener("change", function (event) {
    updatePrice();
  });
  let s = document.getElementsByName("prodType");
  let select = s[0];
  select.addEventListener("change", function (event) {
    updatePrice();
  });
  let radios = document.getElementsByName("prodOptions");
  radios.forEach(function (radio) {
    radio.addEventListener("change", function (event) {
      updatePrice();
    });
  });
  let checkboxes = document.querySelectorAll("#checkboxes input");
  checkboxes.forEach(function (checkbox) {
    checkbox.addEventListener("change", function (event) {
      updatePrice();
    });
  });

  updatePrice();
});


window.addEventListener("DOMContentLoaded", function (event) {
  let b = document.getElementById("my-button");
  b.addEventListener("click", onClick);
});
